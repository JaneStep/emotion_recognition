# -*- coding: utf-8 -*-
"""
Created on Sat May 13 15:36:19 2017

@author: JaneNote
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Feb 07 11:21:03 2017

@author: JaneNote
"""

import matplotlib.pyplot as plt
from pathlib import Path
import menpo.io as mio
from menpodetect import load_dlib_frontal_face_detector
import pickle
import json
`/
import os
    

with open('fitter.pickle', 'rb') as f:
    fitter = pickle.load(f)
images = 'C:/Users/JaneNote/Downloads/lfpw/testset'
path_to_lfpw = Path(images)


print path_to_lfpw
filename = 'image_0026.png'
image = mio.import_image(path_to_lfpw / filename)
   
image = image.as_greyscale()
      
detect = load_dlib_frontal_face_detector()
        # Detect face
bboxes = detect(image)
        

if len(bboxes) > 0:
            # Fit AAM
    result = fitter.fit_from_bb(image, bboxes[0], max_iters=[15, 5])
    print(result)

points_name = filename.replace('.png', '')
print result.final_shape.as_vector()
pickle_name = filename.replace('png', 'pickle')
with open(images +'/' + pickle_name, 'wb') as f:
    pickle.dump(result, f)
with open(images + '/points_of_' + points_name, mode='w') as f:
    json.dump(result.final_shape.tojson(), f)

plt.subplot(131)
image.view()
bboxes[0].view(line_width=3, render_markers=False)
plt.gca().set_title('Bounding box')

plt.subplot(132)
image.view()
result.initial_shape.view(marker_size=4)
plt.gca().set_title('Initial shape')

plt.subplot(133)
image.view()
result.final_shape.view(marker_size=4, figure_size=(15, 13))
plt.gca().set_title('Final shape')