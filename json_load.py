# -*- coding: utf-8 -*-
"""
Created on Sun May 21 14:36:23 2017

@author: JaneNote
"""
import cv2
import pickle
import matplotlib.pyplot as plt
import path
import json
from pathlib import Path
import menpo.io as mio
from menpodetect import load_dlib_frontal_face_detector
import json
"""
cam = cv2.VideoCapture(0)

cv2.namedWindow("test")
"""
path_to = 'C:/Users/JaneNote/Downloads/lfpw/testset_from_webcam'
path_to_lfpw = Path(path_to)
image = mio.import_image(path_to_lfpw / 'opencv_frame_1.png')
with open(path_to + '/points_of_opencv_frame_1') as json_data:
    d = json.load(json_data)
print d

with open(path_to + '/opencv_frame_1.pickle','rb') as f:
    result = pickle.load(f)
image = image.as_greyscale()
      
detect = load_dlib_frontal_face_detector()
        # Detect face
bboxes = detect(image)
         
print image.diagonal()
plt.subplot(131)
image.view()
bboxes[0].view(line_width=3, render_markers=False)
plt.gca().set_title('Bounding box')
        
plt.subplot(132)
image.view()
result.initial_shape.view(marker_size=4)
plt.gca().set_title('Initial shape')
        
plt.subplot(133)
image.view()
result.final_shape.view(marker_size=4, figure_size=(15, 13))
plt.gca().set_title('Final shape')


print result.final_shape.as_vector()
"""
while True:
    ret, frame = cam.read()
    cv2.imshow("test", frame)
    k = cv2.waitKey(1)
    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    for arr in d['points']:
        x = int(arr[1])
        y = int(arr[0])
        cv2.circle(frame,(x,y), 4, (0,255,0), -1)
    cv2.imshow('frame', frame)
    
cam.release()
cv2.destroyAllWindows()
"""