# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 15:25:49 2017

@author: JaneNote
"""
from os import listdir, path
import itertools
import numpy as np
import math
import pandas
from sklearn import preprocessing
import pickle
"""
f=open('C:/Users/JaneNote/Downloads/lfpw/trainset/image_0001.pts')
"""
path_to_trainset = 'C:\Users\JaneNote\Downloads\lfpw\output'
files = listdir(path_to_trainset)
images = filter(lambda x: x.endswith('.pts'), files)



class_file = open('classes.txt')

"""Data headers:"""
"""
for i in range(0, 68):
    test_file.write('x' + str(i) + ',' + 'y' + str(i) + ',')
test_file.write('class\n')
"""
points_img_x = []
points_img_y = [] 
"""
Записываю координаты точек из trainset в массивы
"""         
for image in images:
    k=0
    f = open(path.join(path_to_trainset, image))
    img_x = []
    img_y = []
    for line in f.readlines():
        if line.startswith('}'):
            break
     
        if k < 3:
            k += 1
        else:
            splitted = line.split(" ")
            img_x.append(float(splitted[0]))
            img_y.append(float(splitted[1]))
            
       
    points_img_x.append(img_x)
    points_img_y.append(img_y)
    f.close()
print len(points_img_x)
print len(points_img_y)     
"""
Вычисление расстояний для обучения выборки
"""

point_distances = []
for (points_x, points_y, class_number) in itertools.izip(points_img_x, points_img_y):
    
    print 'class_number' + class_number
    distances = []
    """
    Приведение расстояний к одному формату
    """
    max_points_x = max(points_x)
    min_points_x = min(points_x)
    max_points_y = max(points_y)
    min_points_y = min(points_y)
    for i in range(0, len(points_x) - 1):
        for j in range(i+1, len(points_x) - 1):
            x_i = points_x[i]/(max_points_x - min_points_x)
            x_j = points_x[j]/(max_points_x - min_points_x)
            y_i = points_y[i]/(max_points_y - min_points_y)
            y_j = points_y[j]/(max_points_y - min_points_y)
            distance = math.sqrt((x_j- x_i)*(x_j - x_i) + (y_j - y_i)*(y_j - y_i))
          
            print 'distance = %f' % distance
            distances.append(distance)
    distances.append(int(class_number))
    point_distances.append(distances)

print point_distances
print len(point_distances)



"""
column_names = []
for i in range(distances.size):
    column_names.append("distance_" + str(i))
column_names.append("class")
angles_df = pandas.DataFrame(point_distances, columns = column_names)
correlator = angles_df.corr()["class"]
"""

img_angles = []
for (points_x, points_y, class_number) in itertools.izip(points_img_x, points_img_y, class_file.readlines()):
    points_for_angles_x = np.array([[points_x[21], points_x[22], points_x[27]], [points_x[22], points_x[42], points_x[43]],[points_x[23], points_x[44], points_x[43]], [points_x[24], points_x[25], points_x[44]],[points_x[26], points_x[44], points_x[45]],[points_x[21], points_x[38], points_x[39]],[points_x[20], points_x[37], points_x[38]], [points_x[19], points_x[18], points_x[37]], [points_x[17], points_x[36], points_x[37]],[points_x[27], points_x[28], points_x[42]], [points_x[27], points_x[39], points_x[28]],[points_x[53], points_x[54], points_x[64]], [points_x[54], points_x[64], points_x[55]], [points_x[54], points_x[64], points_x[55]], [points_x[48], points_x[60], points_x[59]], [points_x[48], points_x[60], points_x[49]]])
    points_for_angles_y = np.array([[points_y[21], points_y[22], points_y[27]], [points_y[22], points_y[42], points_y[43]],[points_y[23], points_y[44], points_y[43]], [points_y[24], points_y[25], points_y[44]],[points_y[26], points_y[44], points_y[45]],[points_y[21], points_y[38], points_y[39]],[points_y[20], points_y[37], points_y[38]], [points_y[19], points_y[18], points_y[37]], [points_y[17], points_y[36], points_y[37]],[points_y[27], points_y[28], points_y[42]], [points_y[27], points_y[39], points_y[28]],[points_y[53], points_y[54], points_y[64]], [points_y[54], points_y[64], points_y[55]], [points_y[54], points_y[64], points_y[55]], [points_y[48], points_y[60], points_y[59]], [points_y[48], points_y[60], points_y[49]]])
    print 'points_x_21 = %f' % points_x[21]
    print 'points_x_22 = %f' % points_x[22]
    print 'points_x_27 = %f' % points_x[27]
    angles = []
    for (t_x, t_y) in itertools.izip(points_for_angles_x, points_for_angles_y):
        t_x_iter = itertools.cycle(t_x)
        t_y_iter = itertools.cycle(t_y)
        for i in range(3):
            a_x = next(t_x_iter) - next(t_x_iter)
            
            b_x = next(t_x_iter) - next(t_x_iter)
            
            a_y = next(t_y_iter) - next(t_y_iter)
            
            b_y = next(t_y_iter) - next(t_y_iter)
            
            cos_alpha = (a_x * b_x + a_y * b_y) / (math.sqrt(a_x*a_x+a_y*a_y)*math.sqrt(b_x*b_x+b_y*b_y))
            
            angles.append(cos_alpha)
    angles.append(int(class_number))
    img_angles.append()
    point_distances.append(angles)



with open('img_angles.pickle', 'wb') as file:
    pickle.dump(img_angles, file)
    
with open('point_distances_and_angles.pickle', 'wb') as file: 
    pickle.dump(point_distances, file)
      
print 'img_angles'
print img_angles

"""
column_names = []
for i in range(points_for_angles_x.size):
    column_names.append("angle_" + str(i))
column_names.append("class")
angles_df = pandas.DataFrame(img_angles, columns = column_names)
correlator = angles_df.corr()["class"]

print correlator

for (name, c) in correlator:
    print name
"""
