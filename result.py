# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 17:48:48 2017

@author: JaneNote
"""
import matplotlib.pyplot as plt
from pathlib import Path
import menpo.io as mio
from menpodetect import load_dlib_frontal_face_detector
import pickle
import json
with open('C:/Users/JaneNote/Downloads/lfpw/testset_for_emotions/image_0028.pickle', 'rb') as f:
    result = pickle.load(f)


print result.final_shape.as_vector()
path_to_lfpw = Path('C:/Users/JaneNote/Downloads/lfpw/testset_for_emotions')
# Load and convert to grayscale
image = mio.import_image(path_to_lfpw / 'image_0028.png')
image = image.as_greyscale()
detect = load_dlib_frontal_face_detector()
# Detect face
bboxes = detect(image)

# Crop the image for better visualization of the result
image = image.crop_to_landmarks_proportion(0.3, group='dlib_0')
bboxes[0] = image.landmarks['dlib_0'].lms




plt.subplot(131)
image.view()
bboxes[0].view(line_width=3, render_markers=False)
plt.gca().set_title('Bounding box')



plt.subplot(132)
image.view()
result.initial_shape.view(marker_size=4)
plt.gca().set_title('Initial shape')

plt.subplot(133)
image.view()
result.final_shape.view(marker_size=4, figure_size=(15, 13))
bboxes[0].view(line_width=3, render_markers=False)
plt.gca().set_title('Final shape')
