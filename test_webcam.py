


import numpy as np
import cv2
import random

cap = cv2.VideoCapture(0)
out = cv2.VideoWriter('output_norm.avi', -1, 20.0, (640,480))
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    k = cv2.waitKey(1)  
    cv2.imshow('frame', frame)
    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    # Our operations on the frame come here
    elif k%256 == 32: 
        for i in range(random.randint(1, 100)):
            print i
            cv2.circle(frame,(i,i), 1, (0,255,0), -1)
        
        for i in range(40):
            out.write(frame)
        while (cv2.waitKey(1) & 0xFF != ord('q')):
            cv2.imshow('frame', frame) 
    
   

# When everything done, release the capture
cap.release()
out.release()
cv2.destroyAllWindows()