# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 21:35:18 2017

@author: JaneNote
"""
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import mean_squared_error

import pickle 
import pandas

import numpy as np
model = LogisticRegression()
with open('point__distances_3em.pickle', 'rb') as file:
    point_distances = pickle.load(file)     
    

column_names = []
all_columns = []
for i in range(len(point_distances[0])-1):
    column_names.append("distance_" + str(i))
    
for i in range(len(point_distances[0])-1):
    all_columns.append("distance_" + str(i))  

all_columns.append("class")
distances_df = pandas.DataFrame(point_distances, columns = all_columns)    
target = "class"

model.fit(distances_df[column_names], distances_df[target])
with open('model.pickle', 'wb') as f:
    pickle.dump(model, f)
    

with open('point_test_3em.pickle', 'rb') as file:
    test_points = pickle.load(file)  
    
test_distances_df = pandas.DataFrame(test_points, columns = all_columns)
predictions = model.predict(test_distances_df[column_names])
print (predictions)
procents = model.predict_proba(test_distances_df[column_names])
print procents.size
print procents[0].size
rows = procents.size / procents[0].size
cols = procents[0].size                              
probability = np.zeros((rows, cols))
for i in range(0, rows - 1):
    for j in range(0, cols - 1):
        probability[i][j] = procents[i][j] * 100 
np.set_printoptions(threshold=np.nan)

print 'probs:'
print probability
print(metrics.classification_report(test_distances_df[target], predictions))
print(metrics.confusion_matrix(test_distances_df[target], predictions))
print mean_squared_error(test_distances_df[target], predictions)
