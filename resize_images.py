# -*- coding: utf-8 -*-
"""
Created on Sat Apr 01 19:09:36 2017

@author: JaneNote
"""
JPG_MASK = ".jpg"
import PIL
from PIL import Image
import os
path = 'C:/Users/JaneNote/Downloads/lfpw/trainset_for_emotions'
file_listing = os.listdir(path)
file_listing.sort()
for image_path in file_listing:
    full_path = path + os.path.sep + image_path 
    if JPG_MASK in image_path:
        img = Image.open(full_path)
        new_width = 450
        wpercent = (new_width/float(img.size[0]))
        new_height = int((float(img.size[1]) * float(wpercent)))
        img = img.resize((new_width, new_height), PIL.Image.ANTIALIAS)
        img = img.save(full_path)