# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 15:25:49 2017

@author: JaneNote
"""
from os import listdir, path
import itertools
import numpy as np
import math
import pandas
from sklearn import preprocessing
import pickle



"""
f=open('C:/Users/JaneNote/Downloads/lfpw/trainset/image_0001.pts')
"""
path_to_trainset = 'C:\Users\JaneNote\Downloads\lfpw\mytestset'
class_file = open('test_classes.txt')
print 'efefef'
files = listdir(path_to_trainset)
print 'hui'
images = filter(lambda x: x.endswith('.pts'), files)
print 'iaowjef'


"""
class_file = open('classes.txt')
"""
"""Data headers:"""
"""
for i in range(0, 68):
    test_file.write('x' + str(i) + ',' + 'y' + str(i) + ',')
test_file.write('class\n')
"""
points_img_x = []
points_img_y = [] 
"""
Записываю координаты точек из trainset в массивы
"""
print 'before for'         
for image in images:
    k=0
    f = open(path.join(path_to_trainset, image))
    img_x = []
    img_y = []
    print 'opened pts file'
    for line in f.readlines():
        if line.startswith('}'):
            break
     
        if k < 3:
            k += 1
        else:
            print 'split coords'
            splitted = line.split(" ")
            img_x.append(float(splitted[0]))
            img_y.append(float(splitted[1]))
            
       
    points_img_x.append(img_x)
    points_img_y.append(img_y)
    f.close()
print len(points_img_x)
print len(points_img_y)     
"""
Вычисление расстояний для обучения выборки
"""

point_distances = []
for (points_x, points_y, class_number) in itertools.izip(points_img_x, points_img_y, class_file.readlines()):
    
   
    distances = []
    """
    Приведение расстояний к одному формату
    """
    max_points_x = max(points_x)
    min_points_x = min(points_x)
    max_points_y = max(points_y)
    min_points_y = min(points_y)
    for i in range(0, len(points_x) - 1):
        for j in range(i+1, len(points_x) - 1):
            x_i = points_x[i]/(max_points_x - min_points_x)
            x_j = points_x[j]/(max_points_x - min_points_x)
            y_i = points_y[i]/(max_points_y - min_points_y)
            y_j = points_y[j]/(max_points_y - min_points_y)
            distance = math.sqrt((x_j- x_i)*(x_j - x_i) + (y_j - y_i)*(y_j - y_i))
          
            print 'distance = %f' % distance
            distances.append(distance)
    distances.append(int(class_number))
    point_distances.append(distances)

print point_distances
print len(point_distances)

with open('test_distances.pickle', 'wb') as file:
    pickle.dump(point_distances, file)  
      

