# -*- coding: utf-8 -*-
"""
Created on Fri May 12 23:18:31 2017

@author: JaneNote
"""
from os import listdir, path
import os
import json

path_to_directory = 'C:/Users/JaneNote/Downloads/lfpw/testset_from_webcam'
path_to = 'C:/Users/JaneNote/Downloads/lfpw/output_trainset/pictures'
files = listdir(path_to_directory)
images = filter(lambda x: x.startswith('point'), files)
 
"""
for image in images: 
    os.rename(path.join(path_to_directory, image), path.join(path_to_directory, image + '.json'))
"""
for image in images: 
    with open(path.join(path_to_directory, image)) as json_data:
        d = json.load(json_data)
        f = open(path.join(path_to_directory, image.replace('.json','.pts')),'w')
        mas = []
        f.write('version: 1' + '\n' + 'npoints: 68' + '\n' + '{' + '\n')
        for arr in d['points']:
            f.write(str(arr[1]) + ' ' + str(arr[0]) + '\n')
            mas.append(arr)
        f.write('}')
        print len(mas)
        f.close()
 