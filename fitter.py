# -*- coding: utf-8 -*-
"""
Created on Wed Feb 08 13:15:00 2017

@author: JaneNote
"""

from menpofit.aam import LucasKanadeAAMFitter, WibergInverseCompositional
from menpofit.aam import PatchAAM
from menpo.feature import fast_dsift
import pickle
with open('train_improve_images.pickle', 'rb') as f:
    training_images = pickle.load(f)

patch_aam = PatchAAM(training_images, group='PTS', patch_shape=[(15, 15), (23, 23)], scales=(0.5, 1.0), holistic_features=fast_dsift,
                     max_shape_components=20, max_appearance_components=150,
                     verbose=True)
with open('aam_model.pickle', 'wb') as f: 
    pickle.dump(patch_aam, f)
fitter = LucasKanadeAAMFitter(patch_aam, lk_algorithm_cls=WibergInverseCompositional,
                              n_shape=[5, 20], n_appearance=[30, 150])
with open('fitter.pickle', 'wb') as f:
    pickle.dump(fitter, f)        
   