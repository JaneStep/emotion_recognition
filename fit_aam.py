# -*- coding: utf-8 -*-
"""
Created on Tue Feb 07 11:21:03 2017

@author: JaneNote
"""

import matplotlib.pyplot as plt
from pathlib import Path
import menpo.io as mio
from menpodetect import load_dlib_frontal_face_detector
import pickle
import json
import os
    
images = 'C:/Users/JaneNote/Downloads/lfpw/testset_for_emotions'
path_to_lfpw = Path(images)
for filename in os.listdir(images):
# Load and convert to grayscale
      
    if ('png' in filename): 
        print filename
       
with open('fitter.pickle', 'rb') as f:
    fitter = pickle.load(f)


print path_to_lfpw
for filename in os.listdir(images):
# Load and convert to grayscale
      
    if ('png' in filename): 
        pickle_name = filename.replace('png', 'pickle')
        """
        if (os.path.isfile(images + '/' + pickle_name)): 
            continue
        """
        print 'starting file %s' % filename
        image = mio.import_image(path_to_lfpw / filename)
        try:
            image = image.as_greyscale()
        except ValueError:
            print('Image %s failed.' % filename)
        detect = load_dlib_frontal_face_detector()
            # Detect face
        bboxes = detect(image)
            
        # Crop the image for better visualization of the result
        
        image = image.crop_to_landmarks_proportion(0.3, group='dlib_0')
        bboxes[0] = image.landmarks['dlib_0'].lms
      
        if len(bboxes) > 0:
            # Fit AAM
            result = fitter.fit_from_bb(image, bboxes[0], max_iters=[15, 5])
            print(result)
            pickle_name = filename.replace('png', 'pickle')
            with open(images +'/' + pickle_name, 'wb') as f:
                pickle.dump(result, f)
            points_name = filename.replace('.png', '')
            with open(images + '/points_of_' + points_name, mode='w') as f:
                json.dump(result.final_shape.tojson(), f)
